package it.uniroma3.siw.progetto;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.siw.progetto.model.Allievo;
import it.uniroma3.siw.progetto.model.Attivita;
import it.uniroma3.siw.progetto.model.Centro;
import it.uniroma3.siw.progetto.model.Responsabile;
import it.uniroma3.siw.progetto.service.AllievoService;
import it.uniroma3.siw.progetto.service.AttivitaService;
import it.uniroma3.siw.progetto.service.CentroService;
import it.uniroma3.siw.progetto.service.ResponsabileService;

@SpringBootApplication
public class ProgettoApplication {

	@Autowired
	private ResponsabileService responsabileService;

	@Autowired
	private CentroService centroService;

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private AllievoService allievoService;

	public static void main(String[] args) {
		SpringApplication.run(ProgettoApplication.class, args);
	}

	@PostConstruct
	public void init() {

		// Centro 1
		Centro centro1 = new Centro("Informatica", "Via Roma", "informatica@informatica.it", "06340912");
		centroService.save(centro1);

		// Centro 2
		Centro centro2 = new Centro("Elettronica", "Via Lazio", "elettronica@elettronica.it", "06340911");
		centroService.save(centro2);

		// responsabile 1
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("1992-30-03");
			Responsabile responsabile1 = new Responsabile(new Long(1), "Tommaso", "Fedeli", "tommaso@formationweb.it", "tommaso",
					"telefono", data, "via Roma");
			responsabile1.setCentro(centroService.findById(centro1.getId()));
			// centro1.setResponsabile(responsabile1);
			responsabileService.save(responsabile1);
		} catch (Exception e) {

		}

		// responsabile 2
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("1996-08-08");
			Responsabile responsabile2 = new Responsabile(new Long(2), "Luigi", "Patti", "luigi@formationweb.it", "luigi", "telefono",
					data, "VIA roMA");
			responsabile2.setCentro(centroService.findById(centro2.getId()));
			// centro2.setResponsabile(responsabile2);
			responsabileService.save(responsabile2);
		} catch (Exception e) {

		}

		// Attivita 1
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("2018-06-13");
			SimpleDateFormat sdfOrario = new SimpleDateFormat("HH:mm");
			Date orario = sdfOrario.parse("10:30");
			Attivita attivita = new Attivita("Sistemi informativi web", data, orario, centro1);
			attivitaService.save(attivita);
		} catch (Exception e) {
			// TODO: handle exception
		}

		// Attivita 2
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("2018-06-15");
			SimpleDateFormat sdfOrario = new SimpleDateFormat("HH:mm");
			Date orario = sdfOrario.parse("11:30");
			Attivita attivita = new Attivita("Analisi e progettazione del software", data, orario, centro1);
			attivitaService.save(attivita);
		} catch (Exception e) {
			// TODO: handle exception
		}

		// allievo 1
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("1990-06-13");
			Allievo allievo = new Allievo("Luigi", "Patti", "ciao@ciao.it", "ciao", "ciao", data, "via roma");
			List<Attivita> lista = allievo.getListaAttivita();
			lista.add((Attivita) attivitaService.findById(new Long(1)));
			allievoService.save(allievo);
		} catch (Exception e) {
			e.getMessage();
		}
		// allievo 1
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse("1995-06-13");
			Allievo allievo = new Allievo("Tommaso", "ciao", "tom@tom.it", "tom", "tom", data, "via roma");
			List<Attivita> lista = allievo.getListaAttivita();
			allievoService.save(allievo);
		} catch (Exception e) {
			e.getMessage();
		}
	}

}