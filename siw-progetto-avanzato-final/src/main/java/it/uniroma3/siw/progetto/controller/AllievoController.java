package it.uniroma3.siw.progetto.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.progetto.model.Allievo;
import it.uniroma3.siw.progetto.model.Attivita;
import it.uniroma3.siw.progetto.validator.AllievoValidator;
import it.uniroma3.siw.progetto.service.AllievoService;
import it.uniroma3.siw.progetto.service.AttivitaService;

@Controller
public class AllievoController {
	@Autowired
	AllievoService allievoService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private AllievoValidator validator;
	
	//implementare codice per verificare non esistenza dell'allievo (anche per responsabile)
	
	@PostMapping("/allievoLogin")
	public String checkAllievoInfo( @ModelAttribute Allievo allievo, BindingResult bindingResult, Model model) {
		this.validator.validateLogin(allievo, bindingResult);
				
		if (bindingResult.hasErrors()) {
			return "errorLoginAllievo"; //in caso di errore 
		} else {
			String email = allievo.getEmail();
			String password = allievo.getPassword();
			if(allievoService.findByEmail(email)==null ) {
				model.addAttribute("notExists", "Email non trovata");
				return "errorLoginAllievo";
			}else {
				String passwordAllievo = allievoService.findByEmail(email).get().getPassword();				
				if(!passwordAllievo.equals(password)){
					model.addAttribute("notExists", "I dati inseriti non corrispondo");
					return "errorLoginAllievo";
				} else {
						
						Allievo allievoLoggato = allievoService.findByEmail(email).get();
						model.addAttribute("allievoLoggato", allievoLoggato);
						List<Attivita> listaAttivita = allievoLoggato.getListaAttivita();
						model.addAttribute("listaAttivita",listaAttivita);
				}
			}
		}
		return "loginAllievoEffettuato";
	}
	
	//merge
	
	@RequestMapping("/allievi")
    public String allievi(Model model) {
        model.addAttribute("allievi", this.allievoService.findAll());
        return "allievoList";
    }
	
	
	@RequestMapping("/addAllievo")
	public String addAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "allievoForm";
	}
	
	@RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable("id") Long id, Model model) {
		model.addAttribute("allievo",this.allievoService.findById(id));
		return "showAllievo";
	}
	
	
	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, 
									Model model, BindingResult bindingResult){
		this.validator.validate(allievo, bindingResult);
		
		if (this.allievoService.alreadyExists(allievo)) {
			model.addAttribute("exists","Allievo gia' registrato ");
			return "allievoForm";
		} else {
			if(!bindingResult.hasErrors()) {
				this.allievoService.save(allievo);
				model.addAttribute("allievi",this.allievoService.findAll());
				return "allievoList";
			}

		}
		return "allievoForm";
	}
	
//	@RequestMapping("/addAssociaAttivita")
//	public String addAssociaAttivita(Model model) {
//		model.addAttribute("allievo", new Allievo());
//		return "allievoForm";
//	}
	
	
	@RequestMapping(value="/allievo/{id}/scegliAttivita", method= RequestMethod.GET)
	public String scegliAttivita(@PathVariable("id") Long id, Model model) {
		//model.addAttribute("nomeAttivita",new String());
		model.addAttribute("attivitas", this.attivitaService.findAll());
		model.addAttribute("allievo", this.allievoService.findById(id));
		return "scegliAttivita";
	}
	
	@RequestMapping(value="/allievo/{id}/associaAttivita", method= RequestMethod.GET)
	public String associaAttivita(@PathVariable("id") Long id, @RequestParam(value="attivita") String attivitaIdString , Model model) {
		Allievo allievo=(Allievo) allievoService.findById(id);
		//System.out.println("il valore è"+ attivitaIdString);
		Long attivitaId = new Long(attivitaIdString);
		//System.out.println("il valore è"+attivitaId.toString());
		Attivita attivita= attivitaService.findById(attivitaId);
		if(allievo.getListaAttivita().contains(attivita)) {
			model.addAttribute("exists","attivita gia associata");
			model.addAttribute("attivitas", this.attivitaService.findAll());
			model.addAttribute("allievo", this.allievoService.findById(id));
			return "scegliAttivita";
		}else if(allievo.getListaAttivita().add(attivita)){
				allievoService.save(allievo);
				model.addAttribute("allievoLoggato",(Allievo)this.allievoService.findById(id));
				return "loginAllievoEffettuato";
		}else {
				return "index";
		}	
	}
	
}