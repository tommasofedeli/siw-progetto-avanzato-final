package it.uniroma3.siw.progetto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

// Login Controller
@Controller
public class MainController {

	// Login
	@RequestMapping("/loginAllievo.html")
	public String login() {
		return "loginAllievo";
	}
	
	@RequestMapping("/loginResponsabile.html")
	public String loginResponsabile() {
		return "loginResponsabile";
	}
	
	// Login error
	@RequestMapping("/errorLoginAllievo.html")
	public String loginErrorA(Model model) {
		model.addAttribute("Error", true);
		return "loginAllievo";
	}
	
	@RequestMapping("/errorLoginResponsabile.html")
	public String loginErrorR(Model model) {
		model.addAttribute("Error", true);
		return "loginResponsabile";
	}
	
}