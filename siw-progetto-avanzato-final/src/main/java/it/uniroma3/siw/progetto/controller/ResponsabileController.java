package it.uniroma3.siw.progetto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.progetto.model.Allievo;
import it.uniroma3.siw.progetto.model.Responsabile;
import it.uniroma3.siw.progetto.service.ResponsabileService;
import it.uniroma3.siw.progetto.validator.ResponsabileValidator;

@Controller
public class ResponsabileController {
	@Autowired
	ResponsabileService responsabileService;

	@Autowired
	ResponsabileValidator validator;

	@PostMapping("/responsabileLogin")
	public String checkResponsabileInfo(@Valid @ModelAttribute Responsabile responsabile, BindingResult bindingResult, Model model) {
		
		this.validator.validate(responsabile, bindingResult);
		
		if (bindingResult.hasErrors()) {
			return "errorLoginResponsabile"; // in caso di errore
		} else {
			String email = responsabile.getEmail();
			String password = responsabile.getPassword();
			if (responsabileService.findByEmail(email) == null ) {
				model.addAttribute("notExists", "L'email non è presente nel database ");
				return "errorLoginResponsabile";
			}else {
				String passwordResponsabile = responsabileService.findByEmail(email).getPassword();
				if(!passwordResponsabile.equals(password)) {
					model.addAttribute("notExists", "I dati inseriti non corrispondo");
					return "errorLoginResponsabile";
				}else{
					Responsabile responsabileLoggato = responsabileService.findByEmail(email);
					model.addAttribute("nomeResponsabile", responsabileLoggato);
				}
			}
		}
		return "operazioniResponsabile";
	}
	
	// merge
	/*
	@RequestMapping("/responsabili")
    public String responsabili(Model model) {
        model.addAttribute("responsabili", this.responsabileService.findAll());
        return "responsabileList";
    }

    @RequestMapping("/addResponsabile")
    public String addResponsabile(Model model) {
        model.addAttribute("responsabile", new Responsabile());
        return "responsabileForm";
    }

    @RequestMapping(value = "/responsabile/{id}", method = RequestMethod.GET)
    public String getResponsabile(@PathVariable("id") Long id, Model model) {
        model.addAttribute("responsabile", this.responsabileService.findById(id));
    	return "showResponsabile";
    }

    @RequestMapping(value = "/responsabile", method = RequestMethod.POST)
    public String newResponsabile(@Valid @ModelAttribute("responsabile") Responsabile responsabile, 
    									Model model, BindingResult bindingResult) {
        this.validator.validate(responsabile, bindingResult);
        
        if (this.responsabileService.alreadyExists(responsabile)) {
            model.addAttribute("exists", "Responsabile already exists");
            return "responsabileForm";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.responsabileService.save(responsabile);
                model.addAttribute("responsabili", this.responsabileService.findAll());
                return "responsabileList";
            }
        }
        return "responsabileForm";
    }*/

}