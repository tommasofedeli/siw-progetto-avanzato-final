package it.uniroma3.siw.progetto.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {
	  
	public Optional<Allievo> findByEmail(String email);
	
}
