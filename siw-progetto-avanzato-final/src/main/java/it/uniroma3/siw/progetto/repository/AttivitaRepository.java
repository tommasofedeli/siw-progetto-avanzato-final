package it.uniroma3.siw.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.progetto.model.Attivita;

@Repository
public interface AttivitaRepository extends CrudRepository<Attivita, Long> {
	
	public List<Attivita> findByNome(String nome);

}
