package it.uniroma3.siw.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.progetto.model.Centro;

@Repository
public interface CentroRepository extends CrudRepository<Centro, Long> {

	public List<Centro> findByTelefono(String telefono);

	public List<Centro> findByNomeAndIndirizzoAndEmailAndTelefono(String nome, String indirizzo, String email, String telefono);

}