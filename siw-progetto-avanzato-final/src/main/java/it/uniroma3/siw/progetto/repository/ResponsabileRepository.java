package it.uniroma3.siw.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.progetto.model.Responsabile;

public interface ResponsabileRepository  extends CrudRepository<Responsabile, Long>{
	Responsabile findByEmail(String email);

	List<Responsabile> findByEmailAndPassword(String email, String password);
	
}
