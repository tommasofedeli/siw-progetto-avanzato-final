package it.uniroma3.siw.progetto.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.progetto.model.Allievo;
import it.uniroma3.siw.progetto.repository.AllievoRepository;

@Service
public class AllievoService {
	
	@Autowired
	private AllievoRepository allievoRepository;
	
    @Transactional
    public void save(final Allievo allievo) {
        this.allievoRepository.save(allievo);
    }
    
    public Optional<Allievo> findByEmail(String email){
    	return this.allievoRepository.findByEmail(email);
    }
    
    public List<Allievo> findAll() {
		return (List<Allievo>) this.allievoRepository.findAll();
	}


	public Object findById(Long id) {
		Optional<Allievo> allievo = this.allievoRepository.findById(id);
		if(allievo.isPresent())
			return allievo.get();
		else 
			return null;
	}
	
	public boolean alreadyExists(Allievo allievo) {
		 if(this.allievoRepository.findByEmail(allievo.getEmail()).isPresent())
			 return true;
		 else
			 return false;
	}
	
}
