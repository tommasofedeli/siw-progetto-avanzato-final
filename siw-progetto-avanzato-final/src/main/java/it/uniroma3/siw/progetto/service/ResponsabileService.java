package it.uniroma3.siw.progetto.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.progetto.model.Responsabile;
import it.uniroma3.siw.progetto.repository.ResponsabileRepository;

@Service
public class ResponsabileService {
	@Autowired
	private ResponsabileRepository responsabileRepository;
	
    @Transactional
    public void save(final Responsabile responsabile) {
        this.responsabileRepository.save(responsabile);
    }
    
    public List<Responsabile> findAll() {
		return (List<Responsabile>) this.responsabileRepository.findAll();
	}
	
	public Responsabile findById(Long id) {
		Optional<Responsabile> responsabile = this.responsabileRepository.findById(id);
		if (responsabile.isPresent()) 
			return responsabile.get();
		else
			return null;
	}
    
    public Responsabile findByEmail(String email){
    	return this.responsabileRepository.findByEmail(email);
    }
    
    public boolean alreadyExists(Responsabile responsabile) {
		List<Responsabile> responsabili = this.responsabileRepository.findByEmailAndPassword(responsabile.getEmail(), responsabile.getPassword());
		if (responsabili.size() > 0)
			return true;
		else 
			return false;
	}	
}
