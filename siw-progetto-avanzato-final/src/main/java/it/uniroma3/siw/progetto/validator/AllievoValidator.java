package it.uniroma3.siw.progetto.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.progetto.model.Allievo;

@Component
public class AllievoValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return Allievo.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataNascita", "required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indirizzo", "required");
	}

	public void validateLogin(Object o, Errors errors) {
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required");        
		 ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required");
	}

}